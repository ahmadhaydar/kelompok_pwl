<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Tugas Mandiri

// Route::get('/', function () {
//     return view('index');
// });

// Route::get('/about', function () {
// 	$nama = 'Ahmad Haydar';
//     return view('about', ['nama' => $nama]);
// });

// Route::get('/mahasiswa', function () {
//  $mahasiswa=[
//  'Hafidz','Rafi','Roberto','Mumtaz'
//  ];
//  return view('mahasiswa',['data'=>$mahasiswa]);
// });

// Route::get('/','App\Http\Controllers\PagesController@home');

//login
Route::get('/','App\Http\Controllers\LoginController@index');
Route::get('/login','App\Http\Controllers\LoginController@login');
Route::post('/user','App\Http\Controllers\LoginController@postLogin');
Route::get('/home','App\Http\Controllers\PagesController@home');
Route::get('/about','App\Http\Controllers\PagesController@about');


//routes Students
Route::get('/students','App\Http\Controllers\StudentsController@index');
Route::get('/students/create','App\Http\Controllers\StudentsController@create');
Route::get('/students/{student}','App\Http\Controllers\StudentsController@show');
Route::post('/students','App\Http\Controllers\StudentsController@store');

//logout
Route::get('/logout','App\Http\Controllers\LoginController@userLogout');
