@extends('layout.main')

@section('title', 'Form Tambah Data Mahasiswa')

@section('container')
<div class="container">
	<div class="row">
		<div class="col-8">
    		<h1 class="mt-3">Form Tambah Data Mahasiswa</h1>

            <form method="post" action="/students/">
            @csrf
                <div class="mb-3">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control @error('nama') is-invalid @enderror"  id="nama" placeholder="Masukkan nama" name="nama">
                    @error('nama')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>
                <div class="mb-3">
                    <label for="nrp">nrp</label>
                    <input type="text" class="form-control @error('nrp') is-invalid @enderror" id="nrp" placeholder="Masukkan nrp" name="nrp">
                    @error('nrp')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>
                <div class="mb-3">
                    <label for="password">password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Masukkan password" name="password">
                    @error('password')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>
                <div class="mb-3">
                    <label for="email">email</label>
                    <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Masukkan email" name="email">
                    @error('email')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>
                <div class="mb-3">
                    <label for="major_id">major_id</label>
                    <input type="text" class="form-control @error('major_id') is-invalid @enderror" id="major_id" placeholder="Masukkan major_id" name="major_id">
                    @error('major_id')<div class="invalid-feedback">{{$message}}</div>@enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
            

            

            
		</div>
    </div>
</div>
@endsection