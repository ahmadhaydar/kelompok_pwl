<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <title>Halaman Login</title>
  </head>
  <body>

        <div class="container">
            <div class="row">
                <div class="col-8">
                    <h1 class="mt-3">Halaman Login</h1>

                    <form method="POST" action="/user">
                    @csrf     
                        <div class="mb-3">
                            <label for="nrp">nrp</label>
                            <input type="nrp" class="form-control @error('nrp') is-invalid @enderror" id="nrp" placeholder="Masukkan nrp" name="nrp">
                            @error('nrp')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                        <div class="mb-3">
                            <label for="password">password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Masukkan password" name="password">
                            @error('password')<div class="invalid-feedback">{{$message}}</div>@enderror
                        </div>
                        <button type="submit" class="btn btn-primary" name="submit">Login</button>

                    </form>
                    

                    

                    
                </div>
            </div>
        </div>
    </body>