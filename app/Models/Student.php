<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;

class Student extends Model
{
    protected $fillable = ['nama','nrp','password','email','jurusan','major_id'];
    protected $primaryKey = 'nrp';
    public function major(){
        return $this->belongsTo('App\Models\Major');
    }
}
