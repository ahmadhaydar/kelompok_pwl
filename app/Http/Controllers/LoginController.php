<?php
namespace App\Http\Controllers;
// require 'functions.php';
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller{

    public function index()
    {
        // $students = Student::all();
        return redirect('login');
    }
    
    public function login()
    {
        // $students = Student::all();
        if (session()->has('user')){
            return redirect('home');
        }
        return view('login');
    }

    public function postLogin(Request $request)
    {
        $students=Student::find($request['nrp']);
        $data = $request->input();
        if(is_null($students)){
            return back()->withErrors(['nrp' => 'The provided credentials do not match our records.']);
        }
        else{
            if($students['password'] == $request['password']){
            $request->session()->put('user',$data['nrp']);
            return redirect('home');}
            else{
                return back()->withErrors(['nrp' => 'The provided credentials do not match our records.']);
            }}
        
        
        
        
        // dd($request->all());
        // $data = $request->input();
        
        // $nrp= $request['nrp'];
        // $password= $request['password'];
        // return redirect('/home');
        
        // if(Auth::attempt($credential)){
        //     $request->session()->put('user',$data['nrp']);
        //     return redirect('/home');
        // }
        // return back()->withErrors(['nrp' => 'The provided credentials do not match our records.']);

        // $result= Student::table('students')->where('nrp', $nrp)->get();
        // echo $sql;
        
        // //cek username
        // if(mysqli_num_rows($sql === 1)){
        //     //cek password
        //     $row = mysqli_fetch_assoc($result);
        //     if (password_verify($password, $row["password"]) ){
        //         $request->session()->put('user', $data['nrp']);        
        //         return redirect('home');
        //         exit;
        //     }
        // }


        
        
        
    }

    public function userLogout(Request $request)
    {
        if (session()->has('user')){
            session()->pull('user');
        }
        return redirect('login');
              
    }
}